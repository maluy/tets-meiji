// Для добавления функционала используем классы с префиксом js, стилизовать по этим классам нельзя
'use strict'
$(document).ready(function(){

	// $('input[type=tel]')
	// 	.inputmask("8 (999) 999 99 99");


	// $('.js-popup-img')
	// 	.magnificPopup({
	// 		type:'image',
	// 		closeOnContentClick: true,
	// 		fixedContentPos: true,
	// 		mainClass: 'mfp-no-margins mfp-with-zoom',
	// 		image: {
	// 			verticalFit: true
	// 		},
	// 		zoom: {
	// 			enabled: true,
	// 			duration: 300
	// 		}
	// 	});
});

$(document).ready(function () {
    //open modal
    $('.js-open-modal').click(function () {
        $('.js-modal-listing').fadeIn()
        $('.js-mask').fadeIn()
        $('body').addClass('overflow')
      })

	// BURGER
	$('.js-burger').click(function () {
        $(this).toggleClass('active')
        $('.js-menu-burger').toggleClass('active')
        $('body').toggleClass('overflow')
      })

      $('.js-mask, .js-modal-close').click(function () {
		$('.js-modal').fadeOut()
		$('.js-mask').fadeOut()
		$('body').removeClass('overflow')
	})
})
